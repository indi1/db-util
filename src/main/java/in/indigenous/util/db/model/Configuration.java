package in.indigenous.util.db.model;

import in.indigenous.util.db.constants.DataFileFormat;
import in.indigenous.util.db.constants.DatabaseType;
import in.indigenous.util.db.metadata.model.DatabaseMetadata;

public class Configuration {

	private String script;

	private String testScript;

	private DatabaseType databaseType;

	private String databaseName;

	private String testDatabaseName;

	private String driver;

	private String connectionUrl;

	private String testConnectionUrl;

	private DatabaseMetadata databaseMetadata;

	private String user;

	private String password;

	private String dataFile;

	private DataFileFormat dataFormat;

	private String testUser;

	private String testPassword;

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public DatabaseType getDatabaseType() {
		return databaseType;
	}

	public void setDatabaseType(DatabaseType databaseType) {
		this.databaseType = databaseType;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}

	public String getConnectionUrl() {
		return connectionUrl;
	}

	public void setConnectionUrl(String connectionUrl) {
		this.connectionUrl = connectionUrl;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTestScript() {
		return testScript;
	}

	public void setTestScript(String testScript) {
		this.testScript = testScript;
	}

	public DatabaseMetadata getDatabaseMetadata() {
		return databaseMetadata;
	}

	public void setDatabaseMetadata(DatabaseMetadata databaseMetadata) {
		this.databaseMetadata = databaseMetadata;
	}

	public String getTestDatabaseName() {
		return testDatabaseName;
	}

	public void setTestDatabaseName(String testDatabaseName) {
		this.testDatabaseName = testDatabaseName;
	}

	public String getDataFile() {
		return dataFile;
	}

	public void setDataFile(String dataFile) {
		this.dataFile = dataFile;
	}

	public DataFileFormat getDataFormat() {
		return dataFormat;
	}

	public void setDataFormat(DataFileFormat dataFormat) {
		this.dataFormat = dataFormat;
	}

	public String getTestConnectionUrl() {
		return testConnectionUrl;
	}

	public void setTestConnectionUrl(String testConnectionUrl) {
		this.testConnectionUrl = testConnectionUrl;
	}

	public String getTestUser() {
		return testUser;
	}

	public void setTestUser(String testUser) {
		this.testUser = testUser;
	}

	public String getTestPassword() {
		return testPassword;
	}

	public void setTestPassword(String testPassword) {
		this.testPassword = testPassword;
	}

}
