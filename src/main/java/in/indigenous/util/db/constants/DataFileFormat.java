package in.indigenous.util.db.constants;

public enum DataFileFormat {

	XML,
	CSV,
	XLS;
}
