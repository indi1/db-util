package in.indigenous.util.db.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.datasource.DelegatingDataSource;

import in.indigenous.util.db.model.Configuration;

public class RunTimeConfigurableDataSource extends DelegatingDataSource {

	private Configuration conf;

	private static final Logger LOG = LogManager.getLogger(RunTimeConfigurableDataSource.class);

	public RunTimeConfigurableDataSource(Configuration conf) {
		this.conf = conf;
	}

	@Override
	public Connection getConnection() {
		try {
			Class.forName(conf.getDriver());
		} catch (ClassNotFoundException e) {
			LOG.error("Error loading database driver", e);
		}
		try {
			return DriverManager.getConnection(conf.getConnectionUrl(), conf.getUser(), conf.getPassword());
		} catch (SQLException e) {
			LOG.error("Error getting database connection", e);
		}
		return null;
	}

}
