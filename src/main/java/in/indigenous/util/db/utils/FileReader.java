package in.indigenous.util.db.utils;

public interface FileReader {
	String readFile(final String filePath);
}
