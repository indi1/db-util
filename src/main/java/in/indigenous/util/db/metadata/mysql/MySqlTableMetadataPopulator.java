package in.indigenous.util.db.metadata.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import in.indigenous.util.db.metadata.TableMetadataPopulator;
import in.indigenous.util.db.metadata.model.ColumnMetadata;
import in.indigenous.util.db.metadata.model.ConstraintMetadata;
import in.indigenous.util.db.metadata.model.TableMetadata;


public class MySqlTableMetadataPopulator implements TableMetadataPopulator {

	private JdbcTemplate template;

	private static final String TABLE_METADATA_SCHEMA = "SELECT COLUMN_NAME,COLUMN_TYPE,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=? AND TABLE_SCHEMA = ?";

	private static final String TABLE_PRI_KEYS = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ? AND COLUMN_KEY = 'PRI'";

	private static final String TABLE_CONSTRAINTS = "SELECT TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME,REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE WHERE REFERENCED_TABLE_SCHEMA = ? AND REFERENCED_TABLE_NAME = ?";

	public MySqlTableMetadataPopulator(final JdbcTemplate template) {
		this.template = template;
	}

	@Override
	public TableMetadata getTableMetadata(final String tableSchema, final String tableName) {
		TableMetadata tableMetadata = new TableMetadata();
		tableMetadata.setTableName(tableName);
		tableMetadata.setColumns(getColumnMetadata(tableSchema, tableName));
		tableMetadata.setPrimaryKeys(getPrimaryKeys(tableSchema, tableName));
		tableMetadata.setConstraints(getConstraints(tableSchema, tableName));
		return tableMetadata;
	}

	private List<ColumnMetadata> getColumnMetadata(final String tableSchema, final String tableName) {
		return template.query(TABLE_METADATA_SCHEMA, new RowMapper<ColumnMetadata>() {

			@Override
			public ColumnMetadata mapRow(ResultSet rs, int row) throws SQLException {
				ColumnMetadata column = new ColumnMetadata();
				column.setColumnName(rs.getString("COLUMN_NAME"));
				column.setColumnType(rs.getString("COLUMN_TYPE"));
				column.setDataType(rs.getString("DATA_TYPE"));
				column.setSize(rs.getString("CHARACTER_MAXIMUM_LENGTH"));
				column.setNullable(BooleanUtils.toBoolean(rs.getString("IS_NULLABLE")));
				return column;
			}

		}, tableName, tableSchema);
	}

	private List<String> getPrimaryKeys(final String tableSchema, final String tableName) {
		return template.query(TABLE_PRI_KEYS, new RowMapper<String>() {

			@Override
			public String mapRow(ResultSet rs, int row) throws SQLException {
				return rs.getString("COLUMN_NAME");
			}

		}, tableSchema, tableName);
	}

	private List<ConstraintMetadata> getConstraints(final String tableSchema, final String tableName) {
		return template.query(TABLE_CONSTRAINTS, new RowMapper<ConstraintMetadata>() {

			@Override
			public ConstraintMetadata mapRow(ResultSet rs, int row) throws SQLException {
				ConstraintMetadata constraint = new ConstraintMetadata();
				constraint.setColumnName(rs.getString("COLUMN_NAME"));
				constraint.setTableName(rs.getString("TABLE_NAME"));
				constraint.setConstraintName(rs.getString("CONSTRAINT_NAME"));
				constraint.setReferencedTableName(rs.getString("REFERENCED_TABLE_NAME"));
				constraint.setReferencedColumnName(rs.getString("REFERENCED_COLUMN_NAME"));
				return constraint;
			}

		}, tableSchema, tableName);
	}

}
