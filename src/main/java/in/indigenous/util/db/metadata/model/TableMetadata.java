package in.indigenous.util.db.metadata.model;

import java.util.List;

public class TableMetadata {

	private String tableName;

	private List<ColumnMetadata> columns;

	private List<String> primaryKeys;

	private List<ConstraintMetadata> constraints;

	public List<ConstraintMetadata> getConstraints() {
		return constraints;
	}

	public void setConstraints(List<ConstraintMetadata> constraints) {
		this.constraints = constraints;
	}

	public List<String> getPrimaryKeys() {
		return primaryKeys;
	}

	public void setPrimaryKeys(List<String> primaryKeys) {
		this.primaryKeys = primaryKeys;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List<ColumnMetadata> getColumns() {
		return columns;
	}

	public void setColumns(List<ColumnMetadata> columns) {
		this.columns = columns;
	}

}
