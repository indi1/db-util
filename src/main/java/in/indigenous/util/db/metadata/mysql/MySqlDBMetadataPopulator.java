package in.indigenous.util.db.metadata.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import in.indigenous.util.db.impl.RunTimeConfigurableDataSource;
import in.indigenous.util.db.metadata.DBMetadataPopulator;
import in.indigenous.util.db.metadata.TableMetadataPopulator;
import in.indigenous.util.db.metadata.model.ColumnMetadata;
import in.indigenous.util.db.metadata.model.ConstraintMetadata;
import in.indigenous.util.db.metadata.model.DatabaseMetadata;
import in.indigenous.util.db.metadata.model.TableMetadata;
import in.indigenous.util.db.model.Configuration;
import in.indigenous.util.db.utils.FileReader;
import in.indigenous.util.db.utils.impl.DefaultFileReader;

public class MySqlDBMetadataPopulator implements DBMetadataPopulator {

	private JdbcTemplate template;

	private TableMetadataPopulator tableMetadataPopulator;

	private FileReader defaultDbFileReader;

	private static final String DB_METADATA_SQL = "SELECT table_name FROM information_schema.tables where table_schema=?";

	private static final String CREATE_TABLE = "CREATE TABLE";

	private static final String SPECIAL_CHAR = "`";

	private static final String PRIMARY_KEY = "PRIMARY KEY";

	private static final String AUTO_INCREMENT = "AUTO_INCREMENT";

	private static final String NOT_NULL = "NOT NULL";

	private static final String FOREIGN_KEY = "FOREIGN KEY";

	private static final String REFERENCES = "REFERENCES";

	@Override
	public DatabaseMetadata getDatabaseMetadata(final Configuration conf) {
		initialize(conf);
		DatabaseMetadata databaseMetadata = new DatabaseMetadata();
		databaseMetadata.setDbName(conf.getDatabaseName());
		List<String> tables = getTableNames(conf.getDatabaseName());
		List<TableMetadata> tableMetadataList = new ArrayList<>();
		for (String table : tables) {
			tableMetadataList.add(tableMetadataPopulator.getTableMetadata(conf.getDatabaseName(), table));
		}
		databaseMetadata.setTables(tableMetadataList);
		return databaseMetadata;
	}

	private void initialize(final Configuration conf) {
		template = new JdbcTemplate(new RunTimeConfigurableDataSource(conf));
		tableMetadataPopulator = new MySqlTableMetadataPopulator(template);
		defaultDbFileReader = new DefaultFileReader();
	}

	private List<String> getTableNames(final String dbName) {
		return template.query(DB_METADATA_SQL, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet rs, int row) throws SQLException {
				return rs.getString("table_name");
			}
		}, dbName);
	}

	@Override
	public DatabaseMetadata getDatabaseMetadataFromScript(final Configuration conf) {
		initialize(conf);
		DatabaseMetadata databaseMetadata = new DatabaseMetadata();
		databaseMetadata.setDbName(conf.getDatabaseName());
		databaseMetadata.setTables(getTableMetadataFromScript(conf.getScript()));
		return databaseMetadata;
	}

	private List<TableMetadata> getTableMetadataFromScript(final String scriptPath) {
		String content = defaultDbFileReader.readFile(scriptPath);
		List<String> tableContents = getTableContent(content);
		List<TableMetadata> tableMetadatas = new ArrayList<>();
		for (String tableContent : tableContents) {
			TableMetadata tableMetadata = new TableMetadata();
			tableMetadata.setTableName(getTableName(tableContent));
			tableMetadata.setColumns(getColumnMetadataFromScript(tableContent));
			tableMetadata.setPrimaryKeys(getPrimaryKeys(tableContent));
			tableMetadata.setConstraints(getConstratint(getTableName(tableContent), tableContent));
			tableMetadatas.add(tableMetadata);
		}
		return tableMetadatas;
	}

	private List<ColumnMetadata> getColumnMetadataFromScript(final String tableContent) {
		List<ColumnMetadata> columnMetadata = new ArrayList<>();
		for (String columnContent : getColumnContents(tableContent)) {
			columnMetadata.add(getColumnMetadata(columnContent));
		}
		return columnMetadata;
	}

	private ColumnMetadata getColumnMetadata(final String column) {
		ColumnMetadata columnMetadata = new ColumnMetadata();
		columnMetadata.setColumnName(getColumnName(column));
		if (column.contains(AUTO_INCREMENT)) {
			columnMetadata.setAutoIncrement(true);
		}
		columnMetadata.setColumnType(getColumnType(column));
		columnMetadata.setDataType(getDataType(column));
		if (!column.contains(NOT_NULL)) {
			columnMetadata.setNullable(true);
		}
		columnMetadata.setSize(getColumnSize(column));
		return columnMetadata;
	}

	private String getColumnName(final String column) {
		int start = column.indexOf(SPECIAL_CHAR);
		int end = StringUtils.indexOf(column, SPECIAL_CHAR, 1);
		return column.substring(start, end);
	}

	private String getColumnType(final String column) {
		String columnType = column.trim().split(" ")[1];
		if (columnType.indexOf("(") >= 0) {
			columnType = columnType.substring(0, columnType.indexOf("("));
		}
		return columnType;
	}

	private String getDataType(final String column) {
		// NOT IMPLEMENTED.
		return null;
	}

	private String getColumnSize(final String column) {
		String columnType = column.trim().split(" ")[1];
		if (columnType.indexOf("(") >= 0) {
			return columnType.substring(columnType.indexOf("("), columnType.indexOf(")"));
		}
		return null;
	}

	private List<String> getPrimaryKeys(final String tableContent) {
		String primaryKeyContent = getPrimaryContent(tableContent);
		if (StringUtils.isNotEmpty(primaryKeyContent)) {
			String content = primaryKeyContent.substring(primaryKeyContent.indexOf("("),
					primaryKeyContent.indexOf(")"));
			List<String> pks = new ArrayList<>();
			for (String pk : content.split(",")) {
				pks.add(StringUtils.remove(pk, SPECIAL_CHAR));
			}
			return pks;
		}
		return null;
	}

	private String getPrimaryContent(final String tableContent) {
		int start = tableContent.indexOf("(");
		int end = getIndexWhereClosed(tableContent, "(", ")");
		String mainContent = tableContent.substring(start, end);
		for (String temp : mainContent.split(",")) {
			if (temp.contains(PRIMARY_KEY)) {
				return temp.trim();
			}
		}
		return null;
	}

	private List<ConstraintMetadata> getConstratint(final String table, final String tableContent) {
		List<String> content = getConstraintContent(tableContent);
		List<ConstraintMetadata> metadata = new ArrayList<>();
		for (String consContent : content) {
			metadata.addAll(getConstraintMetadata(table, consContent));
		}
		return metadata;
	}

	private List<ConstraintMetadata> getConstraintMetadata(final String table, final String content) {
		List<String> columnNames = getConstraintColumnName(content);
		List<String> referencedColumnNames = getReferencedColumnName(content);
		if (columnNames.size() != referencedColumnNames.size()) {
			return null;
		}
		List<ConstraintMetadata> list = new ArrayList<>();
		for (String columnName : columnNames) {
			ConstraintMetadata metadata = new ConstraintMetadata();
			metadata.setColumnName(columnName);
			metadata.setConstraintName(getConstraintName(content));
			metadata.setConstraintType(getConstraintType(content));
			metadata.setReferencedColumnName(referencedColumnNames.get(columnNames.indexOf(columnName)));
			metadata.setReferencedTableName(getReferencedTableName(content));
			metadata.setTableName(table);
			list.add(metadata);
		}
		return list;
	}

	private List<String> getConstraintColumnName(final String content) {
		List<String> names = new ArrayList<>();
		int begin = content.indexOf("(");
		int end = content.indexOf(")");
		String columnNameContent = content.substring(begin, end);
		for (String column : columnNameContent.split(",")) {
			names.add(StringUtils.remove(column, SPECIAL_CHAR).trim());
		}
		return names;
	}

	private String getConstraintName(final String content) {
		// Not implemented.
		return null;
	}

	private String getConstraintType(final String content) {
		return content.substring(0, content.indexOf("(")).trim();
	}

	private List<String> getReferencedColumnName(final String content) {
		List<String> names = new ArrayList<>();
		String refinedContent = content.substring(content.indexOf(")"));
		int begin = refinedContent.indexOf("(");
		int end = refinedContent.indexOf(")");
		String columnNameContent = refinedContent.substring(begin, end);
		for (String column : columnNameContent.split(",")) {
			names.add(StringUtils.remove(column, SPECIAL_CHAR).trim());
		}
		return names;
	}

	private String getReferencedTableName(final String content) {
		return content.substring(content.indexOf(REFERENCES), content.lastIndexOf(")")).trim();
	}

	private List<String> getConstraintContent(final String tableContent) {
		List<String> content = new ArrayList<>();
		int start = tableContent.indexOf("(");
		int end = getIndexWhereClosed(tableContent, "(", ")");
		String mainContent = tableContent.substring(start, end);
		for (String temp : mainContent.split(",")) {
			if (temp.contains(FOREIGN_KEY)) {
				content.add(temp.trim());
			}
		}
		return null;
	}

	private List<String> getTableContent(final String content) {
		List<String> tableContents = new ArrayList<>();
		while (content.indexOf(CREATE_TABLE) >= 0) {
			String tableContent = content.substring(content.indexOf(CREATE_TABLE));
			tableContents.add(tableContent.substring(0, getIndexWhereClosed(tableContent, "(", ")")));
			tableContent = tableContent.substring(getIndexWhereClosed(tableContent, "(", ")"));
		}
		return tableContents;
	}

	private int getIndexWhereClosed(final String content, final String open, final String close) {
		int index = -999;
		index = content.indexOf(open);
		int counter = 0;
		if (index >= 0) {
			counter = 1;
			index++;
		}
		while (counter != 0) {
			String check = String.valueOf(content.charAt(index));
			if (check.equals(open)) {
				counter++;
			}
			if (check.equals(close)) {
				counter--;
			}
			index++;
		}
		return index;
	}

	private String getTableName(final String tableContent) {
		int start = tableContent.indexOf(SPECIAL_CHAR);
		String temp = tableContent.substring(start + 1);
		return tableContent.substring(start, start + temp.indexOf(SPECIAL_CHAR));
	}

	private List<String> getColumnContents(final String tableContent) {
		int start = tableContent.indexOf("(");
		int end = getIndexWhereClosed(tableContent, "(", ")");
		String mainContent = tableContent.substring(start, end);
		List<String> columnContent = new ArrayList<>();
		for (String temp : mainContent.split(",")) {
			if (!temp.contains(PRIMARY_KEY) && !temp.contains(FOREIGN_KEY)) {
				columnContent.add(temp.trim());
			}
		}
		return columnContent;
	}

}
