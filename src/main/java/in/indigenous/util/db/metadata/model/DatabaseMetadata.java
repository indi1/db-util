package in.indigenous.util.db.metadata.model;

import java.util.List;

public class DatabaseMetadata {
	
	private String dbName;
	
	private List<TableMetadata> tables;

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public List<TableMetadata> getTables() {
		return tables;
	}

	public void setTables(List<TableMetadata> tables) {
		this.tables = tables;
	}
	
}
