package in.indigenous.util.db.metadata;

import in.indigenous.util.db.metadata.model.DatabaseMetadata;
import in.indigenous.util.db.model.Configuration;

public interface DBMetadataPopulator {
	
	DatabaseMetadata getDatabaseMetadata(final Configuration conf);
	
	DatabaseMetadata getDatabaseMetadataFromScript(final Configuration conf);

}
