package in.indigenous.util.db.metadata;

import in.indigenous.util.db.metadata.model.TableMetadata;

public interface TableMetadataPopulator {
	
	TableMetadata getTableMetadata(final String tableSchema, final String tableName);

}
